@extends('layouts.master')

@section('title')
Newsfeed
@stop

@section('currentpage-NEWSFEED')
<span class="sr-only">(current)</span>
@stop

@section('content')

    <div class="col-sm-3">
        <div class="input-column">
            <label for="postName">Name</label>
            <input class="form-control" type="text" name="name" id="postName" placeholder="John Doe"> 
            <br/>
            <label for="postContent">Content</label>
            <textarea class="form-control" rows="4" id="postContent" placeholder="Enter your post..."></textarea>
            <br/>
            <button type="submit" class="btn btn-primary">Submit</button>
            <br/><br/>
            <!-- Small modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Show random number</button>
            
            <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <?php 
                            $randN = rand(1, 10);
                            echo("Random number is $randN");
                        ?>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="col-sm-9">
        <?php 
            for ($i = 0; $i < $randN; $i++) {
                $image = $posts[$i]['image'];
                $date = $posts[$i]['date'];
                $content = $posts[$i]['content'];
                echo "<div class='post'>";
                echo "<img class='post-image' src='$image'/>";
                echo "<p class='post-date'>$date</p>";
                echo "<p class='post-content'>$content</p>";
                echo "</div> <!-- /post #$i -->";
            }
        ?>
    </div>

@stop