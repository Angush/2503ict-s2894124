<?php 

function getPosts() {
    $posts = array(
        array(
            'date'=>'14/03/2016, 12:13 PM', 
            'content'=>'Hey guys, I just got this new dog! He&#39s an Alaskan Malamute x German Shepherd, and from henceforth, he shall be known only as Scruffy the Magnificent!', 
            'image'=>'images/dog.jpg'
        ), 
        array(
            'date'=>'13/03/2016, 4:55 PM',
            'content'=>'So I went to pick my son up from preschool earlier today, and I get a big ol&#39 hug from some random kid, for absolutely no discernible reason. His (single) mom saw this exchange, and lo and behold, we&#39re having dinner next Friday. I had no idea who you were before this, little friend, but I have decided, after much thought, to erect a temple in your honor. Aw yeah.',
            'image'=>'images/idonteven.jpg'
        ),
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'God, I am a beautiful man.',
            'image'=>'images/handsome.jpg'
        ), 
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'A mural I just did. So I can&#39t draw. Sue me.',
            'image'=>'images/art.jpg'
        ), 
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'Honestly? Even <em>I</em> don&#39t know what this picture is.',
            'image'=>'images/beach.jpg'
        ), 
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'Just bought a new camera. But I still have my old one. You know what that means... camera-ception!',
            'image'=>'images/camera.jpg'
        ), 
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'My wife has gone insane. There are collages everywhere. EVERYWHERE! AAAAAAAARGGGHHHH!!',
            'image'=>'images/collage.jpg'
        ), 
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'I managed to get a picture of my retina from my optometrist. Always wanted to see my own eyes up-close-n-personal.',
            'image'=>'images/eye.jpg'
        ), 
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'Spent the weekend at my grandparent&#39s ranch out in Missouri. Don&#39t blame me for the less-than-perfect picture. My little sis took it on her eye-pod or whatever the kids are callin&#39 them these days. Oh, wonderful. Now I feel old.',
            'image'=>'images/horses.jpg'
        ), 
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'I&#39m gonna be totally honest here. I got lost on my way to Starbucks. Like, seriously lost. If none of you ever see me again, know that I have probably joined one of those desert gangs from Mad Max.',
            'image'=>'images/hills.jpg'
        ) 
    );
    
    return $posts;
}
    
?>
