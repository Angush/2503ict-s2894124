<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

require "models/posts.php";

Route::get('/', function() {
    $posts = getPosts();
	return View::make('pages.newsfeed')->with('posts',$posts);
});

Route::get('friends', function() {
   return View::make('pages.friends');
});
