<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"> 
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <style>
        thead td {
            background-color: lightgrey;
        }
        table {
            border-collapse: collapse;
        }
    </style>
</head>
<body>
    <h1>Hello World</h1>
    <h2>List:</h2>
    <ol>
        <li>Item 1</li>
        <li>Item 2</li>
        <li>etc...</li>
    </ol>
    <table border="1">
        <thead>
            <td>Col 1</td>
            <td>Col 2</td>
        </thead>
        <tbody>
            <tr>
                <td>so</td>
                <td>on</td>
            </tr>
            <tr>
                <td>so</td>
                <td>forth</td>
            </tr>
        </tbody>
    </table>
    <br/>
    <img src="http://i.imgur.com/wZnzJ8D.jpg" width="150" height="150"/>
    <address>P. Sherman, 42 Wallaby Way.</address>
    <p>Click <a href="page2.php">here</a> to go to Page 2!</p>
</body>
</html> 