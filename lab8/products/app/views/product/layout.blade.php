<!DOCTYPE html>
<html>
<head>
    <title>Lab 8 - Migration to View</title>
    {{ HTML::style('css/style.css') }}
</head>
<body>
    <div class="container">

        @yield('content')

    </div> <!-- /container -->
</body>
</html>
