<?php

class ProductSeeder extends Seeder {
    
    public function run() {
        $chocolate_egg = new Product;
        $chocolate_egg->name = 'Chocolate Egg';
        $chocolate_egg->price = 2.99;
        $chocolate_egg->save();
        
        $doritos_170g = new Product;
        $doritos_170g->name = 'Doritos 170g';
        $doritos_170g->price = 3.69;
        $doritos_170g->save();
    }
    
}

?>