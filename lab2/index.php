<?php 
    $posts = array(
        array(
            'date'=>'14/03/2016, 12:13 PM', 
            'content'=>'Hey guys, I just got this new dog! He&#39s an Alaskan Malamute x German Shepherd, and from henceforth, he shall be known only as Scruffy the Magnificent!', 
            'image'=>'images/dog.jpg'
        ), 
        array(
            'date'=>'13/03/2016, 4:55 PM',
            'content'=>'So I went to pick my son up from preschool earlier today, and I get a big ol&#39 hug from some random kid, for absolutely no discernible reason. His (single) mom saw this exchange, and lo and behold, we&#39re having dinner next Friday. I had no idea who you were before this, little friend, but I have decided, after much thought, to erect a temple in your honor. Aw yeah.',
            'image'=>'images/idonteven.jpg'
        ),
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'God, I am a beautiful man.',
            'image'=>'images/handsome.jpg'
        ), 
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'A mural I just did. So I can&#39t draw. Sue me.',
            'image'=>'images/art.jpg'
        ), 
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'Honestly? Even <em>I</em> don&#39t know what this picture is.',
            'image'=>'images/beach.jpg'
        ), 
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'Just bought a new camera. But I still have my old one. You know what that means... camera-ception!',
            'image'=>'images/camera.jpg'
        ), 
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'My wife has gone insane. There are collages everywhere. EVERYWHERE! AAAAAAAARGGGHHHH!!',
            'image'=>'images/collage.jpg'
        ), 
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'I managed to get a picture of my retina from my optometrist. Always wanted to see my own eyes up-close-n-personal.',
            'image'=>'images/eye.jpg'
        ), 
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'Spent the weekend at my grandparent&#39s ranch out in Missouri. Don&#39t blame me for the less-than-perfect picture. My little sis took it on her eye-pod or whatever the kids are callin&#39 them these days. Oh, wonderful. Now I feel old.',
            'image'=>'images/horses.jpg'
        ), 
        array(
            'date'=>'14/03/2016, 6:27 PM',
            'content'=>'I&#39m gonna be totally honest here. I got lost on my way to Starbucks. Like, seriously lost. If none of you ever see me again, know that I have probably joined one of those desert gangs from Mad Max.',
            'image'=>'images/hills.jpg'
        ) 
    );
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Social Network - 2503ict/lab2</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
  </head>

  <body>
    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Social Network</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="./">Home <span class="sr-only">(current)</span></a></li>
              <li><a href="../navbar-static-top/">Photos</a></li>
              <li><a href="../navbar-fixed-top/">Friends</a></li>
              <li><a href="../navbar-fixed-top/">Login</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main body -->
      <div class="row">
        <div class="col-sm-3">
            <div class="input-column">
                <label for="postName">Name</label>
                <input class="form-control" type="text" name="name" id="postName" placeholder="John Doe"> 
                <br/>
                <label for="postContent">Content</label>
                <textarea class="form-control" rows="4" id="postContent" placeholder="Enter your post..."></textarea>
                <br/>
                <button type="submit" class="btn btn-primary">Submit</button>
                <br/><br/>
                <!-- Small modal -->
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm">Show random number</button>
                
                <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <?php 
                            $randN = rand(1, 10);
                            echo("Random number is $randN");
                        ?>
                    </div>
                  </div>
                </div>
                
            </div>
        </div>
        <div class="col-sm-9">
            <?php 
                for ($i = 0; $i < $randN; $i++) {
                    $image = $posts[$i]['image'];
                    $date = $posts[$i]['date'];
                    $content = $posts[$i]['content'];
                    echo "<div class='post'>";
                    echo "<img class='post-image' src='$image'/>";
                    echo "<p class='post-date'>$date</p>";
                    echo "<p class='post-content'>$content</p>";
                    echo "</div> <!-- /post #$i -->";
                }
            ?>
        </div>
      </div>

    </div> <!-- /container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    
  </body>
</html>