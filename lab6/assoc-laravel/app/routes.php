<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
require "models/pms.php";


// Display search form
Route::get('/', function() {
		return View::make('pms.query');
});

// Perform search and display results
Route::get('search', function() {
  	$searchterm = Input::get('searchterm');

  	$results = search($searchterm);

	return View::make('pms.results')->with('pms',$results)->with('searchterm',$searchterm);
});


/* Functions for PM database example. */

/* Search sample data for $searchterm from form. */
function search($searchterm) {
  	$pms = getPms();
	
    // Filter $pms by $searchterm
    if (!empty($searchterm)) {
		$sqlquery = "SELECT * FROM pms 
		  WHERE name LIKE ?
		  OR state LIKE ? 
		  OR party LIKE ?
		  OR start LIKE ?
		  OR finish LIKE ?;";
		$pms = DB::select($sqlquery, array("%$searchterm%","%$searchterm%","%$searchterm%","%$searchterm%","%$searchterm%"));
    }

  return $pms;
}