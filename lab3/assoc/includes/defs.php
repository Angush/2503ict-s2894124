<?php
/* Functions for PM database example. */

/* Load sample data, an array of associative arrays. */
include "pms.php";

/* Search sample data for $name or $year or $state from form. */
function search($searchterm) {
    global $pms; 

    // Filter $pms by $searchterm
    if (!empty($searchterm)) {
		$results = array();
		foreach ($pms as $pm) {
	    	if (stripos($pm['from'], $searchterm) !== FALSE || 
	       		stripos($pm['to'], $searchterm) !== FALSE || 
	        	stripos($pm['name'], $searchterm) !== FALSE || 
		   		stripos($pm['state'], $searchterm) !== FALSE) {
					$results[] = $pm;
		   	}
		}
		$pms = $results;
    }

    return $pms;
}
?>
