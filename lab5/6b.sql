SELECT c.name
    FROM Customers c, Orders o
    WHERE c.id = o.custid
    AND o.itemid IN 
        (SELECT id 
            FROM Stock s
            WHERE s.name = "Fred's Fries");

-- b) What are the names of customers who have bought Fred's Fries?
