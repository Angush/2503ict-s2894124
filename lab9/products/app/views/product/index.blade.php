@extends('product.layout')

@section('content')
    <h1>Products</h1>
@if ($products)
    <table id="products_table">
        <thead>
            <td class="col_name">Name</td>
            <td class="col_price">Price</td>
        </thead>
        <tbody>
@foreach ($products as $product)
            <tr id="product_{{{ $product->id }}}" class="product_row">
                <td class="col_name">{{ link_to_route('product.show', $product->name, array($product->id)) }}</td> <!-- product name -->
                <td class="col_price">${{{ $product->price }}}</td> <!-- product price -->
            </tr> <!-- /product_{{{ $product->id }}} -->
@endforeach
        </tbody>
    </table> <!-- /products_list -->
@else
    <p>No products found.</p>
@endif
@stop