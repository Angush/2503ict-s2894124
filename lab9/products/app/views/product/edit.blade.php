@extends('product.layout')

@section('content')
    <h1>Edit Product</h1>
    
    {{ Form::model($product, array('method' => 'PUT', 'route' => array('product.update', $product->id))) }}
    <!-- Name input -->
        {{ Form::label('name', 'Name: ') }}
        {{ Form::text('name', $product->name) }}
        <span class="error">{{ $errors->first('name') }}</span>
    <br><br>
    <!-- Price input -->
        {{ Form::label('price', 'Price: ') }}
        {{ Form::text('price', $product->price) }}
        <span class="error">{{ $errors->first('price') }}</span>
    <br><br>
    {{ Form::submit('Save') }}
    {{ Form::close() }}
    
    <p>{{ link_to_route('product.show', '&lt; Back to Product Listing', array($product->id)) }}</p>
@stop