<!DOCTYPE html>
<html>
<head>
    <title>Lab 9 - Forms and Validation</title>
    {{ HTML::style('css/style.css') }}
</head>
<body>
    <div class="container">

        @yield('content')

    </div> <!-- /container -->
</body>
</html>
