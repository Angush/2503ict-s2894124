@extends('product.layout')

@section('content')
    <h1>Product Listing</h1>
    <p><span class="bold">Name:  </span>{{{ $product->name }}}</p>
    <p><span class="bold">Price: </span>{{{ $product->price }}}</p>
    <p>{{ link_to_route('product.edit', 'Edit', array($product->id)) }}</p>
    <p>{{ link_to_route('product.index', '&lt; Back to Index') }}</p>
@stop