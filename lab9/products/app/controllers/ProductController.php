<?php

class ProductController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$products = Product::all();
		return View::make('product.index')->with('products', $products);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$product = Product::find($id);
		return View::make('product.show')->with('product', $product);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$product = Product::find($id);
 		return View::make('product.edit')->with('product', $product);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		$product = Product::find($id);
		$form_input = Input::all();
		$validator = Validator::make($form_input, Product::$rules);
 		if ($validator->passes()) {
			$product->name = $form_input['name'];
        	$product->price = $form_input['price'];
			$product->save();
			return Redirect::route('product.show', $product->id);
 		} else {
			return Redirect::route('product.edit', $product->id)->withErrors($validator);
 		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		//
	}


}
